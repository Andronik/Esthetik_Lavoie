function search() {
    var input, filter, array, textValue;
    input = jQuery("#recherche");
    filter = input.val().toUpperCase();
    array = jQuery(".search_div");
    for (i = 0; i < array.length; i++) {
        textValue = array[i].dataset.searchvalue;
        if (textValue.toUpperCase().indexOf(filter) > -1) {
            array[i].style.display = "";
        } else {
            array[i].style.display = "none";
        }
    }
}
