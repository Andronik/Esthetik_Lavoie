<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::redirect('/', '/home');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/reglages', [App\Http\Controllers\ReglagesController::class, 'index'])->name('reglages')->middleware('admin');
Route::get('/articles', [App\Http\Controllers\ArticlePageController::class, 'index'])->name('articles');
Route::get('/services/{id}', [App\Http\Controllers\ServicePageController::class, 'index'])->name('services');
Route::get('/communication', [App\Http\Controllers\CommunicationPageController::class, 'index'])->name('communication');
Route::get('/panier', [App\Http\Controllers\PanierController::class, 'index'])->name('panier')->middleware('auth');
Route::get('/panier/ajout-article/{article}', [App\Http\Controllers\PanierController::class, 'ajouterArticle'])->name('panier.ajouterArticle')->middleware('auth');
Route::get('/panier/ajout-service/{service}/{vehicule}', [App\Http\Controllers\PanierController::class, 'ajouterService'])->name('panier.ajouterService')->middleware('auth');
Route::get('/panier/supprimer/{itemId}', [App\Http\Controllers\PanierController::class, 'destroy'])->name('panier.destroy')->middleware('auth');

Route::resource('article', 'App\Http\Controllers\ArticleController')->middleware('admin');
Route::resource('service', 'App\Http\Controllers\ServiceController')->middleware('admin');
Route::resource('notification', 'App\Http\Controllers\NotificationController')->middleware('admin');
Route::resource('historiqueVentes', 'App\Http\Controllers\HistoriqueVentesController')->middleware('admin');
Route::resource('vehicule', 'App\Http\Controllers\VehiculeController')->middleware('admin');
