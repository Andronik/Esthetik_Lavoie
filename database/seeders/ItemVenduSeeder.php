<?php

namespace Database\Seeders;

use App\Models\HistoriqueVentes;
use App\Models\ItemVendu;
use App\Models\Paiement;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ItemVenduSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('items_vendus')->delete();
        $p = Paiement::all();
        $hv = HistoriqueVentes::all();

        $iv = new ItemVendu();
        $iv->nom = "Decontamination de la peinture";
        $iv->description = "Decontamination des surface peinte. Enlève la poussière de frein ( pico de rouille)";
        $iv->prix = 15;
        $iv->taxe = 2.7;
        $iv->type_item = "Service";
        $iv->paiement()->associate($p->random());
        $iv->historiqueVentes()->associate($hv->random());
        $iv->save();

        $iv = new ItemVendu();
        $iv->nom = "Shampoing des tapis";
        $iv->description = "Shampoing de tapis";
        $iv->prix = 43;
        $iv->taxe = 3.7;
        $iv->type_item = "Service";
        $iv->paiement()->associate($p->random());
        $iv->historiqueVentes()->associate($hv->random());
        $iv->save();

        $iv = new ItemVendu();
        $iv->nom = "Protège-volant";
        $iv->description = "En cuire et facile à poser sur le volant";
        $iv->prix = 10;
        $iv->taxe = 2.5;
        $iv->type_item = "Article";
        $iv->paiement()->associate($p->random());
        $iv->historiqueVentes()->associate($hv->random());
        $iv->save();
    }
}
