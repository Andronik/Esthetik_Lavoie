<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Reservation;
use App\Models\Service;
use App\Models\User;

class ReservationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('reservations')->delete();


        //$u = User::all();

        $user1 = User::where('name', '=', 'admin')->first();
        $user2 = User::where('name', '=', 'client')->first();

        $s = Service::all();

        $r = new Reservation();
        $r->service()->associate($s->random());
        $r->user()->associate($user1);
        $r->save();

        $r = new Reservation();
        $r->service()->associate($s->random());
        $r->user()->associate($user2);
        $r->save();

        $r = new Reservation();
        $r->service()->associate($s->random());
        $r->user()->associate($user2);
        $r->save();
    }
}
