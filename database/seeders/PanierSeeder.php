<?php

namespace Database\Seeders;

use App\Models\Panier;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PanierSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('paniers')->delete();
       // $u = User::all();

        $user1 = User::where('name', '=', 'admin')->first();
        $user2 = User::where('name', '=', 'client')->first();

        $p = new Panier();
        $p->user()->associate($user1);
        $p->prix_total= 145;
        $p->est_paye = true;
        $p->save();

        $p = new Panier();
        $p->user()->associate($user2);
        $p->prix_total= 85;
        $p->est_paye = true;
        $p->save();

        $p = new Panier();
        $p->user()->associate($user1);
        $p->prix_total= 34;
        $p->est_paye = true;
        $p->save();
    }
}
