<?php

namespace Database\Seeders;

use App\Models\Article;
use App\Models\ArticlePanier;
use App\Models\Panier;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ArticlePanierSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('article_paniers')->delete();
        $a = Article::all();
        $p = Panier::all();

        $ap = new ArticlePanier();
        $ap->quantite = 5;
        $ap->article()->associate($a->random());
        $ap->panier()->associate($p->random());
        $ap->save();

        $ap = new ArticlePanier();
        $ap->quantite = 4;
        $ap->article()->associate($a->random());
        $ap->panier()->associate($p->random());
        $ap->save();

        $ap = new ArticlePanier();
        $ap->quantite = 7;
        $ap->article()->associate($a->random());
        $ap->panier()->associate($p->random());
        $ap->save();

        $ap = new ArticlePanier();
        $ap->quantite = 1;
        $ap->article()->associate($a->random());
        $ap->panier()->associate($p->random());
        $ap->save();
    }
}
