<?php

namespace Database\Seeders;

use App\Models\Paiement;
use App\Models\Panier;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PaiementSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('paiements')->delete();

        $pan= Panier::all();

        $P = new Paiement();
        $P->info_compte="compte:N2566467 à jour";
        $P->panier()->associate($pan->random());
        $P->save();

        $P = new Paiement();
        $P->info_compte = "compte:N372365 à jour";
        $P->panier()->associate($pan->random());
        $P->save();

        $P = new Paiement();
        $P->info_compte = "compte:N459365699 à jour";
        $P->panier()->associate($pan->random());
        $P->save();
    }
}
