<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemsVendusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items_vendus', function (Blueprint $table) {
            $table->id();
            $table->string('nom', '32');
            $table->longText('description', '255');
            $table->integer('prix');
            $table->decimal('taxe');
            $table->string('type_item', '255');
            $table->unsignedBigInteger('paiement_id');
            $table->unsignedBigInteger('historique_ventes_id');
            $table->timestamps();
            $table->foreign('paiement_id')->references('id')->on('paiements')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('historique_ventes_id')->references('id')->on('historique_ventes')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items_vendus');
    }
}
