<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreService extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nom' => 'required|min:1|max:32',
            'description'=> 'required|min:1|max:255',
            'prix'=> 'required|numeric|gte:0',
            'image'=> 'required|min:5|max:255',
        ];
    }
}
