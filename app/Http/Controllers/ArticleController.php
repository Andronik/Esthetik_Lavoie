<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreArticle;
use App\Models\Article;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return
     */
    public function index()
    {
       $articles = Article::all();
       return view('article.index', compact('articles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return
     */
    public function create()
    {
        $fichiers=$this->fichiers();
        $articles = Article::all();
        return view('article.create', compact('articles','fichiers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return
     */
    public function store(StoreArticle $request)
    {
        $a= new Article();
        $a->nom = $request->nom;
        $a->description = $request->description;
        $a->quantite_disponible = $request->quantite_disponible;
        $a->prix = $request->prix;;
        $a->image = $request->image;;
        $a->save();
        return redirect('article');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return
     */
    public function show($id)
    {
        $fichiers=$this->fichiers();
        $article = Article::findOrFail($id);
        return view('article.show', compact('article', 'fichiers'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return
     */
    public function edit($id)
    {
        $fichiers=$this->fichiers();
        $article = Article::findOrFail($id);//TODO ajouter une validation
        return view('article.edit', compact('article','fichiers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param StoreArticle $request
     * @param int $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(StoreArticle $request, $id)
    {
        $a= Article::findOrFail($id);
        $a->nom = $request->nom;
        $a->description = $request->description;
        $a->quantite_disponible = $request->quantite_disponible;
        $a->prix = $request->prix;;
        $a->image = $request->image;;
        $a->save();
        return redirect('article');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return
     */
    public function destroy($id)
    {
        $a = Article::findOrFail($id);//TODO ajouter une validation
        $a->delete();
        return redirect('article');
    }

    /**
     * Retourne une liste de fichiers.
     * @param
     * @return array
     */
    public function fichiers(){
        $fichiers=[];
        $fichiers1 = glob('Images/*.{jpg,jpeg,png,gif,svg}', GLOB_BRACE);
        $arrlength = count($fichiers1);
        for($x=0; $x<$arrlength;$x++) {
            $fichiers[explode("/", $fichiers1[$x])[1]] = explode("/", $fichiers1[$x])[1];
        }
        return $fichiers;
    }
}
