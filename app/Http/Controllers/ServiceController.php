<?php

namespace App\Http\Controllers;

use \Illuminate\Contracts\Foundation\Application;
use \Illuminate\Contracts\View\Factory;
use \Illuminate\Contracts\View\View;
use \Illuminate\Http\Response;
use \Illuminate\Http\RedirectResponse;
use \Illuminate\Routing\Redirector;
use App\Http\Requests\StoreService;
use App\Models\Service;


class ServiceController extends Controller
{
    /**
     * Affiche une liste de la ressource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|Response
     */
    public function index()
    {
        $services = Service::all();
        return view('service.index', compact('services'));
    }

    /**
     * Affiche le formulaire de création d'une nouvelle ressource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|Response
     */
    public function create()
    {
        $fichiers = $this->fichiers();
        $services = Service::all();
        return view('service.create', compact('services','fichiers'));
    }

    /**
     * Stocke une ressource nouvellement créée dans le stockage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|Response|\Illuminate\Routing\Redirector
     */
    public function store(StoreService $request)
    {
        $s= new Service();
        $s->nom = $request->nom;
        $s->description = $request->description;
        $s->image = $request->image;
        $s->prix = $request->prix;
        $s->save();
        return redirect('service');
    }

    /**
     * Affiche la ressource spécifiée.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|Response
     */
    public function show($id)
    {
        $fichiers = $this->fichiers();
        $service = Service::findOrFail($id);
        return view('service.show', compact('service','fichiers'));
    }

    /**
     * Affiche le formulaire pour modifier la ressource spécifiée.
     *
     * @param  int  $id
     * @return
     */
    public function edit($id)
    {
        $fichiers = $this->fichiers();
        $service = Service::findOrFail($id);//TODO ajouter une validation
        return view('service.edit', compact('service','fichiers'));
    }

    /**
     * Mets à jour la ressource spécifiée dans le stockage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|Response|\Illuminate\Routing\Redirector
     */
    public function update(StoreService $request, $id)
    {
        $s= Service::findOrFail($id);
        $s->nom = $request->nom;
        $s->description = $request->description;
        $s->image = $request->image;
        $s->prix = $request->prix;
        $s->save();
        return redirect('service');
    }

    /**
     * Supprime la ressource spécifiée du stockage.
     *
     * @param  int  $id
     * @return Application|RedirectResponse|Response|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $s = Service::findOrFail($id);//TODO ajouter une validation
        $s->delete();
        return redirect('service');
    }

    /**
     * Retourne une liste de fichiers.
     * @param
     * @return array
     */
    public function fichiers(){
        $fichiers=[];
        $fichiers1 = glob('Images/*.{jpg,jpeg,png,gif,svg}', GLOB_BRACE);
        $arrlength = count($fichiers1);
        for($x=0; $x<$arrlength;$x++) {
            $fichiers[explode("/", $fichiers1[$x])[1]] = explode("/", $fichiers1[$x])[1];
        }
        return $fichiers;
    }

}
