<?php

namespace App\Http\Controllers;

use App\Models\Service;
use App\Models\ServiceVehicule;
use App\Models\Vehicule;
use Illuminate\Http\Request;

/**
 * Class ServicePageController
 * @package App\Http\Controllers
 *
 * Page qui affiche les services offerts selon le type de véhicule sélectionné
 */
class ServicePageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function index($id)
    {
        if ($id!=0) {
            $v = Vehicule::findOrFail($id);
        } else {
            $v = $id;
        }
        $services = Service::all();
        $vehicules = Vehicule::all();
        return view('servicePage.index', compact('services', 'vehicules', 'v'));
    }
}
