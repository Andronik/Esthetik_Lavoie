<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreHistoriqueVentes;
use App\Models\HistoriqueVentes;
use Illuminate\Http\Request;

class HistoriqueVentesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $historiquesVentes = HistoriqueVentes::all();//TODO ajouter une validation
        return view('historiqueVentes.index', compact('$historiquesVentes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $historiquesVentes = HistoriqueVentes::all();//TODO ajouter une validation
        return view('historiqueVentes.create', compact('historiquesVentes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreHistoriqueVentes $request)
    {
        $h= new HistoriqueVentes();
        $h->confirmation_square = $request->confirmation_square;
        $h->save();
        return redirect('historiqueVentes');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $historiqueVentes = HistoriqueVentes::find($id);
        return view('historiqueVentes.show', compact('historiqueVentes'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $historiqueVentes = HistoriqueVentes::find($id);
        return view('historiqueVentes.edit', compact('historiqueVentes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreHistoriqueVentes $request, $id)
    {
        $h= HistoriqueVentes::find($id);
        $h->confirmation_square = $request->confirmation_square;
        $h->save();
        return redirect('historiqueVentes');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $h = HistoriqueVentes::find($id);//TODO ajouter une validation
        $h->delete();
        return redirect('historiqueVentes');
    }
}
