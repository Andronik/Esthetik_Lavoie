<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\ArticlePanier;
use App\Models\Panier;
use App\Models\Service;
use App\Models\Vehicule;
use Darryldecode\Cart\Cart;
use Illuminate\Http\Request;

class PanierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $itemsPanier = \Cart::session(auth()->id())->getContent();
        return view('panier.index', compact('itemsPanier'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function ajouterArticle(Article $article)
    {
        $a = Article::findOrFail($article->id);

        \Cart::session(auth()->id())->add(array(
            'id' => uniqid(),
            'name' => $a->nom,
            'price' => $a->prix,
            'quantity' => 1,
            'associatedModel' => $a
        ));
        return back()->with('success',"L'article a bien été ajouté");
    }

    /**
     * Update the specified resource in storage.
     *
     * @param $service
     * @return \Illuminate\Http\Response
     */
    public function ajouterService(Service $service, Vehicule $vehicule)
    {
        $s = Service::findOrFail($service->id);
        $v = Vehicule::findOrFail($vehicule->id);

        \Cart::session(auth()->id())->add(array(
            'id' => uniqid(),
            'name' => $s->nom,
            'price' => $s->prix + $v->prix,
            'quantity' => 1,
            'associatedModel' => $s
        ));
        return back()->with('success','Le service a bien été ajouté');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($itemId)
    {
        \Cart::session(auth()->id())->remove($itemId);
        return back();
    }

    public function nombreArticle()
    {
        $x= \Cart::session(auth()->id())->getTotalQuantity();
        return $x;
    }
}
