<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $guarded = array('id');
    /**
     * @var mixed
     */

    public function articlePanier()
    {
        return $this->hasMany(ArticlePanier::class);
    }
}
