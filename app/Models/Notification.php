<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $guarded = array('id');

    public function utilisateurNotification()
    {
        return $this->hasMany(UtilisateurNotification::class);
    }
}
