<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $guarded = array('id');

    public function reservation()
    {
        return $this->hasMany(Reservation::class);
    }

    public function servicePanier()
    {
        return $this->hasMany(ServicePanier::class);
    }
}
