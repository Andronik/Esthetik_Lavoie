<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ItemVendu extends Model
{
    protected $guarded = array('id');
    protected $table = 'items_vendus';

    public function paiement()
    {
        return $this->belongsTo(Paiement::class);
    }

    public function historiqueVentes()
    {
        return $this->belongsTo(HistoriqueVentes::class);
    }
}
