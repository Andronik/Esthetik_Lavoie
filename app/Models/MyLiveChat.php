<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MyLiveChat extends Model
{
    protected $guarded = array('id');
    protected $table = 'my_live_chat';

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
