@extends('layouts.app')
@section('content')
    <div class="container">
        <section class="section-padding">
            <div class="jumbotron" style="padding-top: 15px; background-color: #50B3E5">
                <div class="d-flex justify-content-between">
                    <h1>Services</h1>
                    @if (session('success'))
                        <div class="alert alert-success text-sm-center w-25 p-3 p-3 mb-2 bg-secondary text-white" role="alert">
                            {{ session('success') }}
                            <button class="btn-info rounded-lg" onClick="window.location.reload();"><img src="/Images/reload.svg"></button>
                        </div>
                    @endif
                    <div class="dropdown form-group">
                        <select class="form-control btn-dark" type="dropdown-toggle" id="id" name="id"
                                onchange="top.location.href = this.options[this.selectedIndex].value">
                            @if ($v == '0')
                                <option disabled selected value="">Sélectionner type de vehicule</option>
                            @else
                                <option>{{ $v->type }}</option>
                            @endif
                            @foreach($vehicules as $vehicule)
                                @if ($vehicule != $v)
                                <option class="id" value="{{ route('services', ['id' => $vehicule->id]) }}">
                                    {{ $vehicule->type }}
                                </option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="d-flex justify-content-end">
                    @auth
                        @if (Auth::user()->role==1)
                            <a href="{{ route('service.create') }}" class="btn btn-primary text-justify">Ajouter un service</a>
                        @endif
                    @endauth
                </div>
                @foreach($services as $service)
                    <div class="card mt-3" style="background-color: #C4E0FF">
                        <div class="card-body">
                            <div class="d-flex justify-content-between">

                                <h4 class="card-title">{{ $service->nom }}</h4>
                                @if ($v != '0')
                                    <h3 class="service-prix">{{ $service->prix + $v->prix }}$</h3>
                                @endif
                            </div>
                            <p class="card-text">{{ $service->description }}</p>
                            <img src="{{asset('/Images/'.$service->image)}}" class="img-thumbnail" alt="logo" style="width: 10%;">
                            <div class="text-right">
                                @auth
                                    @if (Auth::user()->role == 1)
                                        <a href="{{ route('service.edit', [$service->id]) }}" class="card-link pr-3">Modifier</a>
                                    @endif
                                @endauth
                                @if (auth()->check() and $v != '0')
                                    <a href="{{ route('panier.ajouterService', [$service->id, $v->id]) }}" class="btn btn-dark">Ajouter au panier</a>
                                @endif
                            </div>

                        </div>

                    </div>
                @endforeach
            </div>
        </section>
    </div>

@stop

