@extends('layouts.app')
@section('content')

    <div class="container">
        <section class="section-padding">
            <div class="jumbotron" style="padding-top: 15px">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a href="{{ route('reglages') }}" style="color: black">
                            <h2>Réglages</h2>
                        </a>
                        <div class="container pt-2">
                            <h3>Édition d'un véhicule</h3>
                            {!! Form::open(['route'=> ['vehicule.update', $vehicule->id], 'method' => 'PUT', 'class' => 'form-horizontal', 'role'=>'form']) !!}
                            @include('vehicule.editForm')
                            <div class="form-group">
                                {!! Form::submit('Sauvegarder', ['class' => 'btn btn-primary']) !!}
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>

            </div>

        </section>
    </div>
@stop
