
@extends('layouts.app')
@section('content')

    <div class="container">
        <section class="section-padding">
            <div class="jumbotron" style="padding-top: 15px">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a href="{{ route('reglages') }}" style="color: black">
                            <h2>Réglages</h2>
                        </a>
                        <div class="container pt-2">
                            <h3>Affichage d'un véhicule</h3>
                            <div class="form-group">
                                {!! Form::label('type', 'Type:', ['class' => "col-sm-2 control-label"]) !!}
                                <div class = 'col-sm-10'>
                                    <input type="text" class="form-control bg-light" id="type" name="type" readonly value="{{ $vehicule->type}}">
                                </div>
                            </div>

                            <div class="form-group">
                                {!! Form::label('prix', 'Prix:', ['class' => "col-sm-2 control-label"]) !!}
                                <div class = 'col-sm-10'>
                                    <input type="text" class="form-control bg-light" id="prix" name="prix" readonly value="{{ $vehicule->prix}}">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </section>
    </div>
@stop
