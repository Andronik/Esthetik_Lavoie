@extends('layouts.app')
@section('content')

    <div class="container">
        <section class="section-padding">
            <div class="jumbotron" style="padding-top: 15px; background-color: #50B3E5">
                <div class="d-flex justify-content-between">
                    <h1>Articles</h1>
                    @if (session('success'))
                        <div class="alert alert-success align-self-center text-sm-center w-25 p-3 p-3 mb-2 bg-secondary text-white" role="alert">
                            {{ session('success') }}
                            <button class="btn-info rounded-lg" onClick="window.location.reload();"><img src="/Images/reload.svg"></button>
                        </div>
                    @endif
                    <div>
                        @auth
                            @if (Auth::user()->role==1)
                                <a href="{{ route('article.create') }}" class="btn btn-primary text-justify">Ajouter un article</a>
                            @endif
                        @endauth
                    </div>
                </div>
            @foreach($articles as $article)
                    <div class="card mt-3" style="background-color: #C4E0FF">
                        <div class="card-body">
                            <div class="d-flex justify-content-between">
                                <h4 class="card-title">{{ $article->nom }}</h4>
                                <h3>{{ $article->prix }}$</h3>
                            </div>
                            <p class="card-text">{{ $article->description }}</p>
                            <img src="{{asset('/Images/'.$article->image)}}" class="img-thumbnail" alt="logo" style="width: 10%;">
                                <div class="text-right">

                                    @auth
                                        @if (Auth::user()->role == 1)
                                            <a href="{{ route('article.edit', [$article->id]) }}" class="card-link pr-3">Modifier</a>
                                        @endif
                                    @endauth
                                    @if ($article->quantite_disponible > 0)
                                        @if(auth()->check())
                                            <a href="{{ route('panier.ajouterArticle', [$article->id]) }}" class="btn btn-dark">Ajouter au panier</a>
                                        @endif
                                    @else
                                        @if(auth()->check())
                                            <a href="#" class="btn btn-dark disabled">Ajouter au panier</a>
                                        @endif
                                        <label class="text-success fw font-weight-bold d-flex justify-content-end" style="margin-bottom: -15px">En rupture de stock...</label>
                                    @endif
                                </div>

                        </div>
                    </div>
                @endforeach
            </div>
        </section>
    </div>

@stop
