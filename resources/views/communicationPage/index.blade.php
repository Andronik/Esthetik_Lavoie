@extends('layouts.app')
@section('content')

    <div class="container">
        <section class="section-padding">
            <div class="jumbotron" style="padding-top: 15px; background-color: #50B3E5">
                <div class="d-flex justify-content-between">
                    <h1>Pour nous joindre</h1>
                    @auth
                        @if (Auth::user()->role==1)
                            <a href="#" class="btn btn-primary text-justify">Modifier</a>
                        @endif
                    @endauth
                </div>
                <div class="card mt-3" style="background-color: #C4E0FF">
                    <div class="card-body">
                        <div id="contact" class="container-fluid bg-grey">
                            <h2 class="text-center">CONTACT</h2>
                            <div class="row">
                                <div class="col-sm-5">
                                    <p>Contactez-nous et nous vous répondrons dans les 24 heures.</p>
                                    <p><span class="fas fa-map-marker-alt"></span> 2415 chemin tourville Drummondville,Qc,Can J2a3y4  </p>
                                    <p><span class="fas fa-phone"></span> +1(819) 991-1991</p>
                                    <p><span class="fas fa-envelope"></span> <span class="text-primary font-weight-bold">esthetiklavoie@gmail.com</span></p>
                                </div>
                                <div class="col-sm-7 slideanim">
                                    <div class="row">
                                        <div class="col-sm-6 form-group">
                                            <input class="form-control" id="nom" name="nom" placeholder="Nom" type="text" required>
                                        </div>
                                        <div class="col-sm-6 form-group">
                                            <input class="form-control" id="email" name="email" placeholder="Email" type="email" required>
                                        </div>
                                    </div>
                                    <textarea class="form-control" id="comments" name="comments" placeholder="Commentaires" rows="5"></textarea><br>
                                    <div class="row">
                                        <div class="col-sm-12 form-group">
                                            <button class="btn btn-success pull-right" type="submit">Envoyer</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Image of location/map -->

                            <img src="Images/esthetik.jpg" class="w3-image w3-greyscale-min center-block" style="width:50%">
                        <hr>

                        <p class="font-weight-bold"> Lien Google maps: <a href="https://www.google.com/maps/dir/45.8941304,-72.5382296/45.81863,-72.4653699/" class="text-justify">https://www.google.com/maps/dir/45.8941304,-72.5382296/45.81863,-72.4653699/</a> </p>
                    </div>
                </div>
            </div>
        </section>
    </div>

@stop
