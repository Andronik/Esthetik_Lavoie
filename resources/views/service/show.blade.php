
@extends('layouts.app')
@section('content')

    <div class="container">
        <section class="section-padding">
            <div class="jumbotron" style="padding-top: 15px">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a href="{{ route('reglages') }}" style="color: black">
                            <h2>Réglages</h2>
                        </a>
                        <div class="container pt-2">
                            <h3>Affichage d'un service</h3>
                            <div class="form-group">
                                {!! Form::label('nom', 'Nom:', ['class' => "col-sm-2 control-label"]) !!}
                                <div class = 'col-sm-10'>
                                    <input type="text" class="form-control bg-light" id="nom" name="nom" readonly value="{{ $service->nom }}">
                                </div>
                            </div>

                            <div class="form-group">
                                {!! Form::label('description', 'Description:', ['class' => "col-sm-2 control-label"]) !!}
                                <div class = 'col-sm-10'>
                                    <input type="text" class="form-control bg-light" id="description" name="description" readonly value="{{ $service->description}}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="fichier" class= "col-sm-10 control-label" >Image:</label>
                                <div class="col-sm-10 control-label">
                                    <input type="text" class="form-control bg-light" id="image" name="image" readonly value="{{ $service->image}}">
                                </div>
                            </div>

                            <div class="form-group">
                                {!! Form::label('prix', 'Prix:', ['class' => "col-sm-10 control-label"]) !!}
                                <div class = 'col-sm-10'>
                                    <input type="text" class="form-control bg-light" id="prix" name="prix" readonly  value="{{ $service->prix}}">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>
    </div>
@stop
