<?php
use App\Models\Service;
if(!isset($service)) {$service= new Service();}
?>

<div class="form-group">
    {!! Form::label('nom', 'Nom:', ['class' => "col-sm-2 control-label"]) !!}
    <div class = 'col-sm-10'>
        {!! Form::text('nom', $service->nom, ['class' => 'form-control']) !!}
        {{ $errors->first('nom') }}
    </div>
</div>

<div class="form-group">
    {!! Form::label('description', 'Description:', ['class' => "col-sm-2 control-label"]) !!}
    <div class = 'col-sm-10'>
        {!! Form::text('description', $service->description, ['class' => 'form-control']) !!}
        {{ $errors->first('description') }}
    </div>
</div>

<div class="form-group">
    <label for="fichier" class= "col-sm-10 control-label" >Image:</label>
    <div class="col-sm-10 control-label">
        {!! Form::select('image', $fichiers, $service->image , ['class' => 'form-control']) !!}
        {{ $errors->first('image') }}
    </div>
</div>

<div class="form-group">
    {!! Form::label('prix', 'Prix:', ['class' => "col-sm-10 control-label"]) !!}
    <div class = 'col-sm-10'>
        {!! Form::text('prix', $service->prix, ['class' => 'form-control']) !!}
        {{ $errors->first('prix') }}
    </div>
</div>






