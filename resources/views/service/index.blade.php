@extends('layouts.app')
@section('content')

    <div class="container">
        <section class="section-padding">
            <div class="jumbotron" style="padding-top: 15px">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a href="{{ route('reglages') }}" style="color: black">
                            <h2>Réglages</h2>
                        </a>
                        <div class="container pt-2">
                            <h3> Liste des services</h3>
                            {{ Form::open(['route'=> ['service.create'], 'role' => 'form', 'method' => 'get', 'class' => 'form-inline']) }}
                            <div class="form-group">
                                <div>
                                    {{ Form::submit('Créer un service', ['class' => 'btn btn-primary'])}}
                                </div>
                            </div>
                            {{ Form::close() }}
                            <br>
                            <div class="form-group">
                                <input type="text" class="form-control" onkeyup="search()" placeholder="Recherche de service(s)..." id="recherche" name="recherche" autocomplete=off>
                            </div>
                            @if ($services->isEmpty())
                                <p> Rien à lister.</p>
                            @else
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Nom</th>
                                        <th>Description</th>
                                        <th>Image</th>
                                        <th>Prix</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($services as $service)
                                        <tr data-searchvalue=" {{$service->nom}} {{$service->description}} {{ $service->prix }} {{ $service->image}}
                                            " class="search_div" >
                                            <td><a href ="{{ route('service.show', [$service->id]) }}"> {{ $service->id }}</a> </td>
                                            <td>{{ $service->nom }}</td>
                                            <td>{{ $service->description }}</td>
                                            <td><img src="{{ asset('Images/'.$service->image)}}" style="max-height: 60px;" alt="photo"/></td>
                                            <td>{{ $service->prix }}</td>
                                            <td><a href="{{ route('service.edit', [$service->id]) }}" class="btn btn-info">Éditer</a></td>
                                            <td>
                                                {{-- Exemple d'un popup de confirmation avant d'effacer
                                                     Le code JS est dans public/js/script.js, qui est inclue dans layout.blade.php --}}
                                                {{ Form::open(array('route' => array('service.destroy', $service->id), 'method' => 'delete', 'data-confirm' => 'Êtes-vous certain?')) }}
                                                <button type="submit" href="{{ URL::route('service.destroy', $service->id) }}" class="btn btn-danger btn-mini">Effacer</button>
                                                {{ Form::close() }}
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

@stop
