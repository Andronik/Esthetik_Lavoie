<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script type="text/javascript" src="{{asset('js/search.js')}}"></script>
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body style="background-color: #0f257b">
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-dark bg-dark shadow-sm">
            <div class="container">
                <div>
                    <img src="/Images/E-L-logo.jpg" align="left" class="rounded-circle" alt="logo" style="max-height: 30px; margin: 4px;">
                    <a class="navbar-brand" href="{{ url('home') }}">
                        Esthetik Lavoie
                        {{--                    {{ config('app.name', 'Laravel') }}--}}
                    </a>
                </div>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
{{--                    <ul class="navbar-nav mr-auto">--}}
{{--                        <a href="services">Service</a>--}}
{{--                        <a href="articles">Articles</a>--}}
{{--                        <a href="communication">Communiquer avec nous</a>--}}
{{--                        <a href="panier">Mon Panier</a>--}}
{{--                    </ul>--}}
                    <ul class="navbar-nav mr-auto">
                    </ul>
                    <div class="navbar-nav mr-auto">
                        <a href="{{ route('services', [$id ?? 0]) }}" class="nav-link">Services</a>
                        <a href="{{ route('articles') }}" class="nav-link">Articles</a>
                        <a href="{{ route('communication') }}" class="nav-link">Nous Joindre</a>
                        @if (Auth()->check())
                            <a href="{{ route('panier') }}" class="nav-link">Mon Panier<sup class="bg-danger rounded-pill"><span class="text-white">{{\Cart::session(auth()->id())->getTotalQuantity()}}</span></sup></a>
                        @else
                            <a href="{{ route('panier') }}" class="nav-link">Mon Panier</a>
                        @endif
                    </div>
                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            @if (Route::has('login'))
                                <li class="nav-item">
{{--                                    <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>--}}
                                    <a class="nav-link" href="{{ route('login') }}">Connexion</a>
                                </li>
                            @endif

                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">Inscription</a>
{{--                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>--}}
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }}
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown" style="background-color: #C4E0FF">
                                    @if (Auth::user()->role == 1)
                                        <a class="dropdown-item" href="{{ route('reglages') }}">Réglages</a>
                                    @endif
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
{{--                                        {{ __('Logout') }}--}}
                                        Déconnexion
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>
</body>
<footer>
    <div class="card-footer fixed-bottom text-center" style="color: #9fcdff">E-mail: <a href="mailto:esthetiklavoie@gmail.com" style="color: #2e9bc9"> <em><b>esthetiklavoie@gmail.com</b></em></a> | Téléphone : <em>(819) 991-1991 </em></div>
</footer>
</html>
