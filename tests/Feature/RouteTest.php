<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class RouteTest extends TestCase
{

    use RefreshDatabase;
    use WithoutMiddleware;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testExample()
    {
        $response = $this->get('/');

        $response->assertStatus(302);
    }

    public function test_visite_page_reglages()
    {
        $response = $this->call('GET', '/reglages');
        $this->assertEquals(200, $response->status());
    }
    public function test_visite_page_services()
    {
        $response = $this->call('GET', '/services');
        $this->assertEquals(200, $response->status());
    }

    public function test_visite_page_articles()
    {
        $response = $this->call('GET', '/articles');
        $this->assertEquals(200, $response->status());
    }

    public function test_visite_Page_communication()
    {
        $response = $this->call('GET', '/communication');
        $this->assertEquals(200, $response->status());
    }

    public function test_visite_Page_panier()
    {
        $response = $this->call('GET', '/panier');
        $this->assertEquals(500, $response->status());
    }


    // Test des Routes CRUD Article


    public function test_article_index()
    {
        $response = $this->call('GET','article');
        $this->assertEquals(200, $response->status());
    }

    public function test_creer_article()
    {
        $response = $this->call('GET','article/create');
        $this->assertEquals(500, $response->status());
    }

    public function test_modifier_article()
    {
            $response = $this->call('PUT','article/{article} ');
        $this->assertEquals(302, $response->status());
    }

    public function test_supprimer_article()
    {
        $response = $this->call('GET',' article/{article} ');
        $this->assertEquals(404, $response->status());
    }



    // Test des Routes CRUD Service

    public function test_service_index()
    {
        $response = $this->call('GET','service');
        $this->assertEquals(200, $response->status());
    }

    public function test_creer_service()
    {
        $response = $this->call('GET','service/create');
        $this->assertEquals(500, $response->status());
    }



    public function test_modifier_service()
    {
        $response = $this->call('PUT','service/{service}');
        $this->assertEquals(302, $response->status());
    }


    public function test_supprimer_service()
    {
        $response = $this->call('GET',' service/{service} ');
        $this->assertEquals(404, $response->status());
    }

}
