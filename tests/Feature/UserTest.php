<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UserTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */

    public function test_pour_acceder_a_la_page_accueil()
    {
        $response = $this->get('home');
        $response ->assertRedirect('login');
    }


}
