<?php

namespace Tests\Feature;

use App\Models\Article;
use App\Models\Service;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class DBsetupTest extends TestCase
{
    use RefreshDatabase;
    /**
     * Test qui Verifie que la BD de test sqlite est vide par defaut
     * @return void
     */
    public function test_Verifie_que_la_bd_de_test_sqlite_est_vide_par_defaut()
    {
        /*
         * Dans l'environnement de test, on crée une BD sqlite :memory:
         * Elle devrait donc être vide en partant.
         */

        $articles = Article::all();
        $services = Service::all();
        $users= User::all();
        $this->assertCount(0,$articles);
        $this->assertCount(0,$services);
        $this->assertCount(0,$users);
    }

    /**
     * Test qui Verifie que la BD de test sqlite peut être "seed"
     * @return void
     */

    public function test_Verifie_que_la_bd_de_test_sqlite_peut_etre_seed()
    {
        //Pour démontrer que le seed fonctionne

        $this->seed();
        $articles = Article::all();
        $services = Service::all();
        $users= User::all();
        $this->assertCount(3,$articles);
        $this->assertCount(3,$services);
        $this->assertCount(2,$users);

    }

    /**
     * Test Verifie que la bd de test sqlite est de nouveau vide apres le test qui la "seedait"
     * @return void
     */

    public function test_Verifie_que_la_bd_de_test_sqlite_est_de_nouveau_vide_apres_le_test_qui_la_seedait()
    {
        //Pour démontrer que le seed fonctionne

        $articles = Article::all();
        $services = Service::all();
        $users= User::all();
        $this->assertCount(0,$articles);
        $this->assertCount(0,$services);
        $this->assertCount(0,$users);
    }
}
