<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;
use App\Models\Article;
class ArticleTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */

    /*
     * Ce qui doit être testé pour le contrôleur d'Article:
     * -on peut créer un article avec de bonnes données x
     * -on peut effacer un article x
     * -on peut modifier les champs d'un article avec de bonnes données x
     * -on peut lister les articles
     * -on peut afficher un article
     * -on ne peut pas afficher un article qui n'est pas dans la bd
     * -on ne peut pas modifier un article qui n'est pas dans la bd
     * -on ne peut pas effacer un article qui n'est pas dans la bd
     * -on ne peut pas créer un article sans nom
     * -on ne peut pas modifier un article sans nom
     *
     * -on ne peut pas effacer un article qui a
     *
     */

    use RefreshDatabase;
    use WithoutMiddleware;

    public function setup() : void {
        parent::setup();
        $this->seed();
    }


    public function test_creer_un_article_avec_de_bonnes_donnees()
    {
        $this->withoutExceptionHandling();
        $this->assertCount(3, Article::all()); // vérification que l'on a 3 articles enregistrés
        $article = [
            'nom' => "article 1",
            'description'=> 'le meilleur des articles',
            'quantite_disponible'=> 8,
            'prix'=> 16.3,
            'image'=> 'article2.png',
        ];
        $response = $this->post('article', $article);
        $this->assertCount(4, Article::all()); // vérification que l'on a 3+1 = 4 articles enregistrés

        $response->assertStatus(302); //ce n'est pas un 200 étant donné qu'on redirect à la fin de Store
        //On fera des tests d'interface plus tard pour vérifier que le redirect va à la bonne place

        $this->assertDatabaseHas('articles',['nom'=>'article 1']);
    }

    public function test_effacer_un_article_qui_est_dans_la_bd() {
        $this->seed('ArticleSeeder');

        $article = Article::all()->first();
        $this->assertCount(3,Article::all()); // Le nombre d'articles enregistré est de 3
        $response = $this->delete('article/'.$article->id);
        $this->assertDatabaseMissing('articles',['id'=>$article->id]);
        $this->assertCount(2,Article::all()); // Le nombre d'articles restants après la suppression d'un article est: 2
    }

    /*
     * Pourquoi tester la modification des champs 1 par 1?
     * Simplement parce qu'il est possible qu'une erreur fasse en sorte qu'un des champs
     * ne puisse être modifié.
     */
    public function test_modifier_les_champs_d_un_article_qui_est_dans_la_bd_avec_de_bonnes_donnes() {
        $this->seed('ArticleSeeder');
        $article = Article::all()->first();
        $article->nom = 'test';
        $article->description = 'un test encours';
        $article->quantite_disponible = 5;
        $article->prix = 6;
        $article->image = 'article1.png';

        $reponse = $this->put('article/'.$article->id,['nom'=>$article->nom,'description' => $article->description, 'quantite_disponible' => $article->quantite_disponible,'prix' => $article->prix, 'image' => $article->image]);
        $reponse->assertStatus(302);
        $articleModifie = Article::findOrFail($article->id);
        $this->assertEquals($article->nom, $articleModifie->nom);
    }

    public function test_article_index()
    {
        $response = $this->call('GET', 'article');

        $this->assertEquals(200, $response->status());
    }

}
