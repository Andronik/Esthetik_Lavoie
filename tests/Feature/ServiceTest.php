<?php

namespace Tests\Feature;

use App\Models\Service;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class ServiceTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */

    /*
     * Ce qui doit être testé pour le contrôleur de service:
     * -on peut créer un service avec de bonnes données x
     * -on peut effacer un service x
     * -on peut modifier les champs d'un service avec de bonnes données x
     * -on peut lister les services
     * -on peut afficher un service
     * -on ne peut pas afficher un service qui n'est pas dans la bd
     * -on ne peut pas modifier un service qui n'est pas dans la bd
     * -on ne peut pas effacer un service qui n'est pas dans la bd
     * -on ne peut pas créer un service sans nom
     * -on ne peut pas modifier un service sans nom
     *
     * -on ne peut pas effacer un service qui a
     *
     */

    use RefreshDatabase;
    use WithoutMiddleware;

    public function setup(): void
    {
        parent::setup();
        $this->seed();
    }

    public function test_creer_un_service_avec_de_bonnes_donnees()
    {
        $this->withoutExceptionHandling();
        $this->assertCount(3, Service::all()); // vérification que l'on a 3 services enregistrés
        $service = [
            'nom' => "service 1",
            'description' => 'le meilleur des services',
            'prix' => 16.3,
            'image' => 'service2.png',
        ];
        $response = $this->post('service', $service);
        $this->assertCount(4, Service::all()); // vérification que l'on a 3+1 = 4 services enregistrés

        $response->assertStatus(302); //ce n'est pas un 200 étant donné qu'on redirect à la fin de Store
        //On fera des tests d'interface plus tard pour vérifier que le "redirect" va à la bonne place

        $this->assertDatabaseHas('services', ['nom' => 'service 1']);
    }

    public function test_effacer_un_service_qui_est_dans_la_bd()
    {
        $this->seed('ServiceSeeder');

        $service = Service::all()->first();
        $this->assertCount(3,Service::all()); // Le nombre de services enregistré est de 3
        $response = $this->delete('service/' . $service->id);
        $this->assertDatabaseMissing('services', ['id' => $service->id]);

        $this->assertCount(2,Service::all()); // Le nombre de services restants après la suppression d'un service est: 2

    }

    /*
     * Pourquoi tester la modification des champs 1 par 1?
     * Simplement parce qu'il est possible qu'une erreur fasse en sorte qu'un des champs
     * ne puisse être modifié.
     */
    public function test_modifier_les_champs_d_un_service_qui_est_dans_la_bd_avec_de_bonnes_donnes()
    {
        $this->seed('ServiceSeeder');
        $service = Service::all()->first();
        $service->nom = 'test';
        $service->description = 'un test encours';
        $service->prix = 6;
        $service->image = 'service1.png';

        $response = $this->put('service/' . $service->id, ['nom' => $service->nom, 'description' => $service->description, 'prix' => $service->prix, 'image' => $service->image]);
        $response->assertStatus(302);
        $serviceModifie = Service::findOrFail($service->id);
        $this->assertEquals($service->nom, $serviceModifie->nom);
    }
}
