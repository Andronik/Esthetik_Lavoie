<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Http\Requests\StoreArticle;
use Tests\TestCase;


class StoreArticleTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @dataProvider provider
     */
    public function test_valeurs_limites_pour_article($nom, $description, $quantite_disponible, $prix, $image, $valide)
    {
        $regles = (new StoreArticle())->rules();
        $data = ['nom' => $nom, 'description' => $description, 'quantite_disponible' => $quantite_disponible,'prix' => $prix, 'image' => $image];
        $validator = app()->get('validator');

        $this->assertEquals($valide, $validator->make($data, $regles)->passes());
    }

    public function provider()
    {
        return [
            'nom à 1'           => ['x',str_repeat('x',5), 1, 1, str_repeat('x', 5), true],
            'nom limite 32'     => [str_repeat('x',32),str_repeat('x',5), 1, 1, str_repeat('x', 5), true],
            'nom trop long 33'  => [str_repeat('x',33), str_repeat('x',5), 1, 1, str_repeat('x', 5), false],
            'nom vide'          => ["",str_repeat('x',5), 1, 1, str_repeat('x', 5), false],
            'nom null'          => [null, str_repeat('x',5),1, 1, str_repeat('x', 5), false],
            'description à 1'           => ['x', str_repeat('x',5),1, 1, str_repeat('x', 5), true],
            'description null'          => ['x', null, 1, 1,str_repeat('x', 5), false],
            'description vide'          => ['x', "", 1, 1, str_repeat('x', 5), false],
            'description limite 256'    => ['x', str_repeat('x',256), 1,1, str_repeat('x', 5), true],
            'description depasse 256'   => ['x', str_repeat('x',257), 1, 1,str_repeat('x', 5), false],
            'quantité numérique valide'     => ['x',str_repeat('x',5), 1,1, str_repeat('x', 5), true],
            'quantité numérique limite 999'   => ['x',str_repeat('x',5), 1, 999, str_repeat('x', 5), true],
            'quantité numérique dépasse 999'   => ['x',str_repeat('x',5), 1, 1000, str_repeat('x', 5), false],
            'quantité numérique non-valide' => ['x',str_repeat('x',5), 1, "D", str_repeat('x', 5), false],
            'quantité null non-valide'      => ['x',str_repeat('x',5), 1, null, str_repeat('x', 5), false],
            'quantité négatif non-valide'   => ['x',str_repeat('x',5), 1, -1, str_repeat('x', 5), false],
            'prix numéric valide'       => ['x',str_repeat('x',5), 1,1, str_repeat('x', 5), true],
            'prix numéric limite 999'       => ['x',str_repeat('x',5), 1,999, str_repeat('x', 5), true],
            'prix numéric dépasse 999'       => ['x',str_repeat('x',5), 1,1000, str_repeat('x', 5), false],
            'prix numéric non-valide'   => ['x',str_repeat('x',5), 1, "D", str_repeat('x', 5), false],
            'prix null non-valide'      => ['x',str_repeat('x',5), 1, null, str_repeat('x', 5), false],
            'prix négatif non-valide'   => ['x',str_repeat('x',5), 1, -1, str_repeat('x', 5), false],
            'image à 5'             => ['x', str_repeat('x',5), 1, 1, str_repeat('x', 5), true],
            'image à 4 non-valide'  => ['x', str_repeat('x',5), 1, 1, str_repeat('x', 4), false],
            'image null non-valide' => ['x', str_repeat('x',5), 1, 1, null, false],
            'image vide non-valide' => ['x',str_repeat('x',5), 1, 1, '', false],
        ];
    }
}
