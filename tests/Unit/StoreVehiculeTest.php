<?php

namespace Tests\Feature;

use App\Http\Requests\StoreService;
use App\Http\Requests\StoreVehicule;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class StoreVehiculeTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @dataProvider provider
     */
    public function test_valeurs_limites_pour_vehicule($type, $prix, $valide)
    {
        $regles = (new StoreVehicule())->rules();
        $data = ['type' => $type, 'prix' => $prix];
        $validator = app()->get('validator');

        $this->assertEquals($valide, $validator->make($data, $regles)->passes());
    }

    public function provider()
    {
        return [
            'nom type à 1'              => ['x',1, true],
            'nom type null non-valide'  => [null,1, false],
            'nom type vide non-valide'  => ["",1, false],
            'prix à 0'                  => ['x',0, true],
            'prix à 1'                  => ['x',1, true],
            'prix négatif non-valide'   => ['x',-1, false],
            'prix non numéric non-valide'   => ['x','x', false],
        ];
    }
}
