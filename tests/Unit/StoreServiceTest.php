<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Http\Requests\StoreService;
use Tests\TestCase;

class StoreServiceTest extends TestCase
{

    use RefreshDatabase;

    /**
     * @dataProvider provider
     */
    public function test_valeurs_limites_pour_service($nom, $description, $prix, $image, $valide)
    {
        $regles = (new StoreService())->rules();
        $data = ['nom' => $nom, 'description' => $description, 'prix' => $prix, 'image' => $image];
        $validator = app()->get('validator');

        $this->assertEquals($valide, $validator->make($data, $regles)->passes());
    }

    public function provider()
    {
        return [
            'nom à 1'           => [str_repeat('x',1), 1, 1, str_repeat('x', 5), true],
            'nom limite 32'     => [str_repeat('x',32), 1, 1, str_repeat('x', 5), true],
            'nom trop long 32'  => [str_repeat('x',33), 1, 1, str_repeat('x', 5), false],
            'nom vide'          => ["", 1, 1, str_repeat('x', 5), false],
            'nom null'          => [null, 1, 1, str_repeat('x', 5), false],
            'prix numéric valide'       => [str_repeat('x',1), 1, 4, str_repeat('x', 5), true],
            'prix numéric non-valide'   => [str_repeat('x',1), 1, "D", str_repeat('x', 5), false],
            'prix null non-valide'      => [str_repeat('x',1), 1, null, str_repeat('x', 5), false],
            'prix négatif non-valide'   => [str_repeat('x',1), 1, -1, str_repeat('x', 5), false],
            'description à 1'           => [str_repeat('x',1), str_repeat('x',1), 1, str_repeat('x', 5), true],
            'description null'          => [str_repeat('x',1), null, 1, str_repeat('x', 5), false],
            'description vide'          => [str_repeat('x',1), "", 1, str_repeat('x', 5), false],
            'description limite 256'    => [str_repeat('x',1), str_repeat('x',256), 1, str_repeat('x', 5), true],
            'description depasse 256'   => [str_repeat('x',1), str_repeat('x',257), 1, str_repeat('x', 5), false],
            'image à 5'             => ['x', 'x', 1, str_repeat('x', 5), true],
            'image à 4 non-valide'  => ['x', 'x', 1, str_repeat('x', 4), false],
            'image null non-valide' => ['x', 'x', 1, null, false],
            'image vide non-valide' => ['x', 'x', 1, '', false],
        ];
    }
}
