### Sommaire

<! - Résumez le bogue rencontré de manière concise. ->

### Étapes à suivre pour reproduire

<! - Décrivez comment on peut reproduire le problème - c'est très important. Veuillez utiliser une liste ordonnée. ->

### Exemple de projet

<! - Si possible, créez un exemple de projet ici sur GitLab.com qui présente le problème
comportement, et créez un lien vers celui-ci ici dans le rapport de bogue. Si vous utilisez une ancienne version de GitLab, ce
déterminera également si le bogue est corrigé dans une version plus récente. ->

### Quel est le lien de la page rattaché au * bug *?

<! - Écrivez le lien ici. ->

### Quel est le comportement actuel du * bug *?

<! - Décrivez ce qui se passe réellement. ->

### Quel est le comportement * correct * attendu?

<! - Décrivez ce que vous devriez voir à la place. ->

### Journaux et / ou captures d'écran pertinents

<! - Collez tous les journaux pertinents - veuillez utiliser des blocs de code (`` '') pour formater la sortie de la console, les journaux et le code
 car il est difficile de lire autrement. ->

### Sortie des chèques

<! - Si vous signalez un bogue sur GitLab.com, écrivez: Ce bogue se produit sur GitLab.com ->

#### Résultats des informations sur l'environnement GitLab

<! - Entrez les informations pertinentes sur l'environnement GitLab si nécessaire. ->

<détails>
<summary> Développer pour la sortie liée aux informations sur l'environnement GitLab </summary>

<pre>

(Pour les installations avec le package omnibus-gitlab, exécutez et collez la sortie de:
`sudo gitlab-rake gitlab: env: info`)

(Pour les installations à partir de la source, exécutez et collez la sortie de:
`sudo -u git -H bundle exec rake gitlab: env: info RAILS_ENV = production`)

</pre>
</details>

#### Résultats de la vérification de l'application GitLab

<! - Saisissez les informations de vérification de l'application GitLab si nécessaire. ->

<détails>
<summary> Développer pour la sortie liée à la vérification de l'application GitLab </summary>
<pre>

(Pour les installations avec le package omnibus-gitlab, exécutez et collez la sortie de:
`sudo gitlab-rake gitlab: check SANITIZE = true`)

(Pour les installations à partir de la source, exécutez et collez la sortie de:
`sudo -u git -H bundle exec rake gitlab: check RAILS_ENV = production SANITIZE = true`)

(nous enquêterons uniquement si les tests réussissent)

</pre>
</details>

### Corrections possibles

<! - Si vous le pouvez, créez un lien vers la ligne de code qui pourrait être responsable du problème. ->

/ label ~ bug

</pre>
</details>

### Possible fixes

<!-- If you can, link to the line of code that might be responsible for the problem. -->

/label ~bug
