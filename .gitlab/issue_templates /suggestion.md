### Sommaire

<! - Résumez votre proposition. ->

### Quel est le lien de la page rattaché à la suggestion?

<! - Écrivez le lien ici. ->

### Quel est la raison de votre proposition?

<! - Décrivez le but de cette proposition. ->

/label ~suggestion
