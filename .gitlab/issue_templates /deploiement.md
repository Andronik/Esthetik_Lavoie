### Sommaire

<! - Résumez le problème survenu lors du déploiement de manière concise. ->

### Quelle technologie avez-vous utilisé pour le déploiement?

<! - Indiquez la technologie utilisée ->

### Avez-vous suivi correctement les différentes étapes dans le guide de déploiement?

<! - Répondez par oui ou non. ->

### Étapes à suivre pour reproduire

<! - Décrivez comment on peut reproduire le problème - c'est très important. Veuillez utiliser une liste ordonnée. ->

### Journaux et / ou captures d'écran pertinents

<! - Collez tous les journaux pertinents - veuillez utiliser des blocs de code (`` '') pour formater la sortie de la console, les journaux et le code
 car il est difficile de lire autrement. ->


/label ~deploiement
