### Sommaire

<! - Résumez le problem rencontré. ->

### Étapes à suivre pour reproduire

<! - Décrivez comment on peut reproduire le problème - c'est très important. Veuillez utiliser une liste ordonnée. ->


### Quel est le lien de la page rattaché au * problem *?

<! - Écrivez le lien ici. ->

### Quel est le comportement actuel du * problem *?

<! - Décrivez ce qui se passe réellement. ->

### Quel est le comportement * correct * attendu?

<! - Décrivez ce que vous devriez voir à la place. ->

### Journaux et / ou captures d'écran pertinents

<! - Collez tous les journaux pertinents - veuillez utiliser des blocs de code (`` '') pour formater la sortie de la console, les journaux et le code
 car il est difficile de lire autrement. ->


### Corrections possibles

<! - Si vous le pouvez, créez un lien vers la ligne de code qui pourrait être responsable du problème. ->

/ label ~ autre

</pre>
</details>

### Possible fixes

<!-- If you can, link to the line of code that might be responsible for the problem. -->

/label ~autre
